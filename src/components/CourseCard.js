import React from 'react';
import { Card, Button } from 'react-bootstrap';


export default function CourseCard({courseProp}) {

	//check to see if the data was successfully passed
	// console.log(props)
	// console.log(typeof props)


	//Deconstruct the courseProp into their own variables
	const { name, description, price } = courseProp;

	return(

		<Card className="m-3">
			<Card.Body>
				<Card.Title> { name } </Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text> { description } </Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php { price }</Card.Text>
				<Button variant="primary">Enroll</Button>
			</Card.Body>
		</Card>

		)
}